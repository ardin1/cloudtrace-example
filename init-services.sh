#!/usr/bin/env bash

set -euo pipefail

go run frontend-service/cmd/main.go &
go run authenticator/cmd/server/main.go &
go run resource/cmd/server/main.go &

for job in `jobs -p`
do
  wait $job
done



cloudtrace-example
===

Code example for  Wartek Tech-Sharing event with title "Distributed Tracing with Cloud Trace".

# Setup

1. Install go
2. Run `go mod tidy`
3. Run `GOOGLE_CLOUD_PROJECT=<your gcp project> make services` to start services
4. Run `make test` to create a tracing

// Copyright 2021 Kementerian Pendidikan dan Kebudayaan Republik Indonesia

package main

import (
	"context"
	"log"
	"math/rand"
	"net"
	"time"

	"gitlab.com/ardin1/cloudtrace-example/pkg/util"
	"gitlab.com/ardin1/cloudtrace-example/rpc"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/trace"
	"google.golang.org/grpc"
)

type Authenticator struct {
	rpc.UnimplementedAuthenticatorServer
}

var _ rpc.AuthenticatorServer = (*Authenticator)(nil)

func (s *Authenticator) Authenticate(ctx context.Context, r *rpc.AuthenticateRequest) (*rpc.AuthenticateResponse, error) {

	// Simulate IO Latency between 10 to 50 ms
	_, span := trace.StartSpan(ctx, "example.Authenticator.Authenticate.ReadIO")
	time.Sleep(time.Duration(10+rand.Intn(40)) * time.Millisecond)
	span.AddAttributes(
		trace.StringAttribute("db", "mysql-auth"),
		trace.BoolAttribute("success", true),
	)
	span.End()

	if r.ClientId == "testing" && r.ClientToken == "testingtoken" {
		return &rpc.AuthenticateResponse{
			Result: rpc.AuthenticateResponse_AUTHENTICATED,
		}, nil
	}

	return &rpc.AuthenticateResponse{
		Result: rpc.AuthenticateResponse_UNAUTHENTICATED,
	}, nil
}

func main() {
	util.InitTrace()

	// Initialize grpc server with tracing support for handler
	srv := grpc.NewServer(grpc.StatsHandler(&ocgrpc.ServerHandler{}))

	rpc.RegisterAuthenticatorServer(srv, &Authenticator{})

	addr := ":9080"
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("gRPC server: failed to listen: %v", err)
	}

	log.Printf("authenticator gRPC server serving at %q", addr)

	if err := srv.Serve(ln); err != nil {
		log.Fatalf("gRPC server: error serving: %v", err)
	}
}

// Copyright 2021 Kementerian Pendidikan dan Kebudayaan Republik Indonesia

package main

import (
	"context"
	"log"
	"math/rand"
	"net"
	"time"

	"github.com/google/uuid"
	"gitlab.com/ardin1/cloudtrace-example/pkg/util"
	"gitlab.com/ardin1/cloudtrace-example/rpc"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/trace"
	"google.golang.org/grpc"
)

type ResourceServer struct {
	rpc.UnimplementedResourceServer
}

var _ rpc.ResourceServer = (*ResourceServer)(nil)

func (s *ResourceServer) Get(ctx context.Context, r *rpc.GetResourceRequest) (*rpc.GetResourceResponse, error) {

	// Simulate IO Latency between 20 to 60 ms
	_, span := trace.StartSpan(ctx, "example.Resource.Get.ReadIO")
	span.AddAttributes(
		trace.StringAttribute("db", "postgres"),
		trace.BoolAttribute("success", true),
	)
	time.Sleep(time.Duration(20+rand.Intn(50)) * time.Millisecond)
	span.End()

	return &rpc.GetResourceResponse{
		ResourceId: uuid.New().String(),
	}, nil
}

func main() {
	util.InitTrace()

	// Initialize grpc server with tracing support for handler
	srv := grpc.NewServer(grpc.StatsHandler(&ocgrpc.ServerHandler{}))

	rpc.RegisterResourceServer(srv, &ResourceServer{})

	addr := ":9082"
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("gRPC server: failed to listen: %v", err)
	}

	log.Printf("Resource gRPC server serving at %q", addr)

	if err := srv.Serve(ln); err != nil {
		log.Fatalf("gRPC server: error serving: %v", err)
	}
}

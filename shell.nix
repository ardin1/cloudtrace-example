with import <nixpkgs> {}; 
{
  devEnv = mkShell {
    buildInputs = [
      go 
      glibc.static 
      gopls 
      go-outline 
      golangci-lint
      delve
      goimports
      protobuf
      grpcurl
      protoc-gen-go
      protoc-gen-go-grpc
    ];
    nobuildPhase = "";
    shellHook = ''
      source .setup-env
    '';
  };
}

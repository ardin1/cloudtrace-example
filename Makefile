
.PHONY: rpc
rpc:
	protoc rpc/defs.proto --go_out=paths=source_relative:. --go-grpc_out=paths=source_relative:.

.PHONY: services
services:
	go mod tidy
	./init-services.sh

.PHONY: test
test:
	curl -H 'X-CLIENT-ID: testing' -H 'X-CLIENT-TOKEN: testingtoken' localhost:9081/get-resource


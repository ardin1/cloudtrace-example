module gitlab.com/ardin1/cloudtrace-example

go 1.16

require (
	contrib.go.opencensus.io/exporter/stackdriver v0.13.10
	github.com/google/uuid v1.1.2
	go.opencensus.io v0.23.0
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)

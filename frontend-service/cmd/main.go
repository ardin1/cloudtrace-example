package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/ardin1/cloudtrace-example/pkg/util"
	"gitlab.com/ardin1/cloudtrace-example/rpc"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/plugin/ochttp"
	"google.golang.org/grpc"
)

func main() {
	util.InitTrace()

	// example a request handler
	getResourceHandler := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {

		// RPC to authentication server
		id := r.Header.Get("X-CLIENT-ID")
		token := r.Header.Get("X-CLIENT-TOKEN")

		cc, err := grpc.Dial(":9080", grpc.WithInsecure(), grpc.WithStatsHandler(new(ocgrpc.ClientHandler)))

		if err != nil {
			util.ReturnHTTPServerError(rw, fmt.Sprintf("Authenticator gRPC client failed to dial to server: %v", err))
			return
		}

		ac := rpc.NewAuthenticatorClient(cc)
		authResp, err := ac.Authenticate(r.Context(), &rpc.AuthenticateRequest{ClientId: id, ClientToken: token})
		if err != nil {
			util.ReturnHTTPServerError(rw, fmt.Sprintf("Failed to call Authenticate RPC: %v", err))
			return
		}

		if authResp.Result == rpc.AuthenticateResponse_UNAUTHENTICATED {
			rw.WriteHeader(http.StatusForbidden)
			rw.Write([]byte("Request denied!"))
			return
		}

		// If authentication succeed, then get the resource!

		cc, err = grpc.Dial(":9082", grpc.WithInsecure(), grpc.WithStatsHandler(new(ocgrpc.ClientHandler)))

		if err != nil {
			util.ReturnHTTPServerError(rw, fmt.Sprintf("Authenticator gRPC client failed to dial to server: %v", err))
			return
		}

		rc := rpc.NewResourceClient(cc)
		res, err := rc.Get(r.Context(), &rpc.GetResourceRequest{ClientId: id})

		if err != nil {
			util.ReturnHTTPServerError(rw, fmt.Sprintf("Failed to call Get Resource RPC: %v", err))
			return
		}

		// Return resource response to caller
		rw.Write([]byte(fmt.Sprintf("Get resource with ID: %v", res.ResourceId)))
		return
	})

	// Wrap handler to add tracing
	och := &ochttp.Handler{
		Handler: getResourceHandler,
	}

	mux := http.NewServeMux()
	mux.Handle("/get-resource", och)

	port := 9081
	log.Printf("Listen and serve request from %d", port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), mux)
}

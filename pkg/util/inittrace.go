package util

import (
	"os"

	"log"

	"contrib.go.opencensus.io/exporter/stackdriver"
	"go.opencensus.io/trace"
)

func InitTrace() error {
	log.Printf("creating trace exporter in project %s.\n", os.Getenv("GOOGLE_CLOUD_PROJECT"))
	exporter, err := stackdriver.NewExporter(stackdriver.Options{
		ProjectID: os.Getenv("GOOGLE_CLOUD_PROJECT"),
	})

	if err != nil {
		// Log only when failed
		log.Fatalf("failed to create stackdriver exporter: %s.\n", err)
	} else {
		// We only register the exporter when we have successfully construct it.
		trace.RegisterExporter(exporter)
	}

	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
	log.Printf("trace created with always sampler")
	return nil
}

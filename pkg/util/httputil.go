package util

import (
	"log"
	"net/http"
)

func ReturnHTTPServerError(rw http.ResponseWriter, reason string) {

	log.Fatalf("Server error: %s", reason)
	rw.WriteHeader(http.StatusInternalServerError)
	rw.Write([]byte("Internal server error!"))

}
